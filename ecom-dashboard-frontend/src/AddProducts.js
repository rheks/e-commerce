import Header from "./Header";
import { useState } from "react";
import { useHistory } from "react-router-dom";

function AddProducts() {
  const history = useHistory();
  const [name, setName] = useState("");
  const [file, setFile] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");

  async function addProduct() {
    // console.warn(name, file, price, description);
    const formData = new FormData();

    formData.append("file", file);
    formData.append("price", price);
    formData.append("name", name);
    formData.append("description", description);

    let result = await fetch("http://localhost:8000/api/addProduct", {
      method: "POST",
      body: formData,
    });
    alert("Product successfully added");
    history.push("/");
  }

  return (
    <div>
      <Header />
      <div className="col-sm-6 offset-sm-3">
        <h1>Add Products</h1>
        <input
          type="text"
          name="Name"
          className="form-control"
          onChange={(e) => setName(e.target.value)}
          placeholder="Masukkan Nama Produk"
        />{" "}
        <br />
        <input
          type="file"
          name="File"
          className="form-control"
          onChange={(e) => setFile(e.target.files[0])}
          placeholder="Masukkan Gambar Produk"
        />{" "}
        <br />
        <input
          type="text"
          name="Price"
          className="form-control"
          onChange={(e) => setPrice(e.target.value)}
          placeholder="Masukkan Harga Produk"
        />{" "}
        <br />
        <textarea
          type="text"
          name="Description"
          className="form-control"
          onChange={(e) => setDescription(e.target.value)}
          placeholder="Masukkan Deskripsi Produk"
        />{" "}
        <br />
        <button onClick={addProduct} className="btn btn-primary">
          Add Product
        </button>
      </div>
    </div>
  );
}

export default AddProducts;
