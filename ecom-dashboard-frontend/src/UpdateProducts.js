import Header from "./Header";
import { withRouter } from "react-router-dom";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

function UpdateProducts(props) {
  const history = useHistory();
  const [name, setName] = useState("");
  const [file, setFile] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");

  const [data, setData] = useState("");
  // console.warn("props", props.match.params.id);

  useEffect(async () => {
    let result = await fetch(
      "http://localhost:8000/api/product/" + props.match.params.id
    );
    result = await result.json();
    setData(result);
    setFile(result.name);
    setName(result.file);
    setPrice(result.price);
    setDescription(result.description);
  }, []);

  async function editProduct(id) {
    // alert(id);

    const formData = new FormData();

    formData.append("file", file);
    formData.append("price", price);
    formData.append("name", name);
    formData.append("description", description);

    let result = await fetch("http://localhost:8000/api/updateproduct/"+ id +"?_method=PUT", {
      method: "POST",
      body: formData,
    });
    alert("Product has been updated");
    history.push("/");
  }

  return (
    <div>
      <Header />
      <div className="col-sm-6 offset-sm-3">
        <h1>Update Product</h1>
        <input
          type="text"
          defaultValue={data.name}
          className="form-control"
          onChange={(e) => setName(e.target.value)}
        />{" "}
        <br />
        <input
          type="text"
          defaultValue={data.price}
          className="form-control"
          onChange={(e) => setPrice(e.target.value)}
        />{" "}
        <br />
        <input
          type="text"
          defaultValue={data.description}
          className="form-control"
          onChange={(e) => setDescription(e.target.value)}
        />{" "}
        <input
          type="file"
          defaultValue={data.file_path}
          className="form-control"
          onChange={(e) => setFile(e.target.files[0])}
        />{" "}
        <br />
        <img
          src={"http://localhost:8000/" + data.file_path}
          style={{ width: 200 }}
        />
        <br />
        <button
          onClick={() => editProduct(data.id)}
          className="btn btn-success"
        >
          Update Product
        </button>
      </div>
    </div>
  );
}

export default withRouter(UpdateProducts);
