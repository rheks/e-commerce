import Header from "./Header";
import React, { useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";

function SearchProduct() {
  const [data, setData] = useState([]);
  let num = 1;

  async function search(key) {
    // console.warn(key);

    let result = await fetch("http://localhost:8000/api/search/" + key);
    result = await result.json();
    setData(result);
  }

  useEffect(() => {
    search();
  }, []);
  // console.warn("result", data);

  async function deleteOperation(id) {
    // alert("Data terhapus");
    if (
      window.confirm(
        "Are you sure want to delete product with id = " + id + " ?"
      )
    ) {
      let result = await fetch("http://localhost:8000/api/delete/" + id, {
        method: "GET",
      });
      result = await result.json();
      // console.warn(result);
      search();
      alert("Product has been deleted");
    } else {
      search();
    }
  }

  return (
    <div>
      <Header />
      <div className="col-sm-6 offset-sm-3">
        <h1>Search Product</h1>
        <input
          type="text"
          className="form-control"
          onChange={(e) => search(e.target.value)}
          placeholder="Masukkan kata kuncu nama produk"
        />
        <Table striped bordered hover variant="light">
          <thead>
            <tr>
              <th>No.</th>
              <th>Name</th>
              <th>Price</th>
              <th>Description</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => (
              <tr>
                <td>{num++}</td>
                <td>{item.name}</td>
                <td>{item.price}</td>
                <td>{item.description}</td>
                <td>
                  <img
                    style={{ width: 100 }}
                    src={"http://localhost:8000/" + item.file_path}
                  ></img>
                </td>
                <td>
                  <Link to={"update/" + item.id}>
                    <button className="btn btn-success">Update</button>
                  </Link>
                  <button
                    className="btn btn-danger"
                    onClick={() => deleteOperation(item.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    </div>
  );
}

export default SearchProduct;
