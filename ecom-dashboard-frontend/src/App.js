import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import AddProducts from "./AddProducts";
import UpdateProducts from "./UpdateProducts";
import Productlist from "./Productlist";
import Protected from "./Protected";
import SearchProduct from "./SearchProduct";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          {/* <Header /> */}
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/add">
            <Protected Cmp={AddProducts} />
            {/* <AddProducts /> */}
          </Route>
          <Route path="/update/:id">
            <Protected Cmp={UpdateProducts} />
            {/* <UpdateProducts /> */}
          </Route>
          <Route path="/search">
            <Protected Cmp={SearchProduct} />
            {/*  */}
          </Route>
          <Route path="/">
            <Protected Cmp={Productlist} />
            {/*  */}
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
