import './App.css'
import { Navbar, NavDropdown } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";

function Header() {
  let user = JSON.parse(localStorage.getItem("user-info"));
  // console.warn(user);

  const history = useHistory();

  function logout() {
    localStorage.clear();
    history.push("/login");
  }

  return (
    <div>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/">E-Commerce</Navbar.Brand>
          <Nav className="me-auto navbar_wrapper">
            {localStorage.getItem("user-info") ? (
              <>
                <Link className="nav_link" to="/">Product List</Link>
                <Link className="nav_link" to="/add">Add Product</Link>
                <Link className="nav_link" to="/update">Update Product</Link>
                <Link className="nav_link" to="/search">Search Product</Link>
              </>
            ) : (
              <>
                <Link className="nav_link" to="/login">Login</Link>
                <Link className="nav_link" to="/register">Register</Link>
              </>
            )}
          </Nav>
          {localStorage.getItem("user-info") ? (
            <Nav>
              <NavDropdown title={user && user.name}>
                <NavDropdown.ItemText onClick={logout}>
                  Logout
                </NavDropdown.ItemText>
              </NavDropdown>
            </Nav>
          ) : null}
        </Container>
      </Navbar>
    </div>
  );
}
// {
//   "name": "Andre",
//   "email": "Andre@gmail.com",
//   "id": 10
// }
export default Header;
